// spec.js
describe('MTT weather app', function() {
    var searchInput = element(by.css('input.add'));
    var addButton = element(by.css('.md-fab'));
    var removeButton = element(by.css('.md-secondary'));
    var city = element.all(by.repeater('city in cities'))
  
    beforeEach(function() {
        browser.get('http://localhost:9000/');
      });

    it('it should increase the amount of cities by 1', function() {
        city.count().then(function(count) {
            searchInput.sendKeys('London');
            addButton.click();
            var newCount = city.count();
            expect(newCount).toEqual(count + 1);
        });
    });

    it('it should decrease the amount of cities by 1', function() {
        city.count().then(function(count) {
            searchInput.sendKeys('London');
            removeButton.click();
            var newCount = city.count();
            expect(newCount).toEqual(count - 1);
        });
    });

    it('it should click on the first city on the list and go to a new page', function() {
        searchInput.sendKeys('Dublin');
        addButton.click();
        city.get(0).element(by.css('.md-primary')).click();
        expect(browser.getCurrentUrl()).toContain("Dublin");
    });



});