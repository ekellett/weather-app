'use strict';

//var app = angular.module('MTTWeatherApp', ['mainService']);

app.controller('MainCtrl', ['$scope', '$location','OpenWeatherApi', function($scope, $location, OpenWeatherApi) {
//get city in url to pass as query in service
$scope.city = $location.url();
//get List from local storage
$scope.savedCityList = localStorage.getItem('cities');
//if list is not null parse stored list or set to empty array
$scope.cities = (localStorage.getItem('cities')!==null) ? JSON.parse($scope.savedCityList) : [];
//set cities to string
localStorage.setItem('cities', JSON.stringify($scope.cities));

//set up base url and parameters
$scope.config = {
	url: 'http://api.openweathermap.org/data/2.5/weather?q=',
    params: {
    	query: $scope.city,
    	apiKey: '0a3276f340a3ec780ac8f22ddd884a4a',
    	units: '&units=metric'
    }
}

	OpenWeatherApi.getData(function(data){
		if(data != 'error' && data.hasOwnProperty('weather')){
			$scope.location = data;
			$scope.iconUrl = 'http://openweathermap.org/img/w/';
		} else {
			$scope.apiError = true;
		}
  }, $scope.config.url+$scope.config.params.query+"&appid="+$scope.config.params.apiKey+$scope.config.params.units);

	//remove city for the list
	$scope.remove = function(city) {
	    var indexOf = $scope.cities.indexOf(city);
	    //if city is inside of the array
	    if (indexOf !== -1) {
	    	//remove city from the array
	      $scope.cities.splice(indexOf, 1);
	      //remove item from local storage
	      localStorage.removeItem(indexOf);
	      //convert to JSON string
	      localStorage.setItem('cities', JSON.stringify($scope.cities));
	    }
	  };

	//add city to a list
	$scope.addCity = function () {
		$scope.alreadyExists = false;
		var indexOf = $scope.cities.indexOf($scope.cities.city);
		//if city is not already inside of the array
	    if (indexOf == -1) {
	    	$scope.cities.push($scope.cities.city);
	    	//clear input field after push
	    	$scope.cities.city = '';
	    	//convert to JSON string
	    	localStorage.setItem('cities', JSON.stringify($scope.cities));
	    } else {
	    	$scope.alreadyExists = true;
	    }
    };
//
}]);