'use strict';

/**
 * @ngdoc overview
 * @name MTTWeatherApp
 * @description
 * # MTTWeatherApp
 *
 * Main module of the application.
 */
var app = angular.module('MTTWeatherApp', ['ngRoute', 'mainService','ngMaterial']);

app.config(['$routeProvider', '$mdThemingProvider', function ($routeProvider, $mdThemingProvider) {
		$routeProvider
			.when('/', {
			    templateUrl: 'app/main/main-view.html',
			    controller: 'MainCtrl'
			})
			.when('/:city', {
			    templateUrl: 'app/main/city-view.html',
			    controller: 'MainCtrl'
			})
			.otherwise({
			    redirectTo: '/'
			});

			//angular material theme
			$mdThemingProvider.theme('default')
			    .primaryPalette('indigo')
			    .accentPalette('pink');
			}
]);
