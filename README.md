# Prerequisites

1. [NodeJS]
2. [Grunt]


# Project setup

I have developed this project using several node modules installed via the node package manager. With nodeJS installed, these dependencies can be installed by running *npm install* from the root project folder.

Grunt has also been implemented for starting up the project on a grunt server at localhost:9000. If you need to install grunt, install nodejs first so that you can run the npm command from the command line. Run *npm install -g grunt-cli* from the command line in the project root folder to install grunt. You can verify that grunt is installed by running *grunt --version* in the command line and seeing the version number being printed

The OpenWeatherAPI is used to get the data for any location including over 200,000 cities.


# Launching the project

With Grunt installed, cd into the project root folder from the command line and run *grunt connect* to launch the application over localhost:9000


# Running unit tests

I have used protractor for  end to end testing for this project. Protractor runs tests against your application running in a real browser, interacting with it as a user would.  You can install Protractor globally by entering *npm install -g protractor* in the command line.  The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running. Use it to download the necessary binaries with: *webdriver-manager update*

Then start up a server with: *webdriver-manager* start. The tests can be executed by cd'ing into the project root folder and running *protractor conf.js*. There will be feedback available in the command line window showing the results of all 3 tests.